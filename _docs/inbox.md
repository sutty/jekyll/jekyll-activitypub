---
title: Inbox
layout: post
---

The [Inbox](https://www.w3.org/TR/activitypub/#inbox) is the dynamic
part of implementing ActivityPub support on a static website, since it
takes care of handling activities sent to the site.

The Inbox is delegated to Social Distributed Press.
