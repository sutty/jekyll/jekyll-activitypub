---
title: Outbox
layout: post
---

The outbox is the collection of all activities.

## Configuration

If your site has many posts, this plugin will generate a paginated
outbox.  By default it will create a page for every 20 activities.

```yaml
# _config.yml
activity_pub:
  items_per_page: 20
```
