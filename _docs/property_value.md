---
title: Property Value
layout: post
---

`PropertyValue`s are used to create tables with information on your
profile, like pronouns, other websites, etc.

By default, your profile will include the website URL.

## Configuration

To change the website name from the default "Website", add a custom name
on `_config.yml`.

```yaml
activity_pub:
  website:
    name: "Visit my website"
```
