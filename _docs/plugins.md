---
title: Plugins
layout: post
---

Plugins are modifications done to activities to support new FEPs,
compatibility with other software, etc.

| Plugin                | Step                     | Hook          | Priority    |
| --------------------- | ------------------------ | ------------- | ----------- |
| Nokogiri              | `:activity`              | `:post_init`  | 100         |
| Absolute assets       | `:activity`              | `:post_init`  | 35          |
| Assets as attachments | `:activity`              | `:post_init`  | 34          |
| Link iframes          | `:activity`              | `:post_init`  | 31          |
| FEP fffd              | `:actor` and `:activity` | `:post_init`  | `:low` (10) |
| FEP fffd: license     | `:activity`              | `:post_init`  | 9           |
| FEP 1042              | `:actor` and `:activity` | `:pre_render` | 8           |

## Nokogiri

Add `jekyll-activity-pub-nokogiri` to your plugins list if you want to
write plugins that modify the contents.

This is a high priority plugin that runs right after each Activity is
instantiated with the convert content from a Jekyll document. A Nokogiri
HTML5 fragment is available to any other hook that comes after it.

## Absolute assets

Enable `jekyll-activity-pub-absolute-assets` if your content includes
assets (images, videos, audios) with absolute or relative paths, so
they're available remotely.

Note Mastodon and other software may remove them via sanitization.

This plugin enables the Nokogiri plugin as a dependency.

## Assets as attachments

Enable `jekyll-activity-pub-assets-as-attachments` to copy embedded
assets as attachments of the activity, so instances like Mastodon can
still show them.

Note the plugin doesn't remove duplicates and Mastodon only supports the
first four attachments.

This plugin enables the Absolute Assets plugin as a dependency.

## Link iframes

Enable `jekyll-activity-pub-link-iframes` if your content embeds remote
sites like videos from Peertube.  The plugin will replace any `<iframe>`
element via a link.

This plugin enables the Nokogiri plugin as a dependency.

## [FEP fffd](https://codeberg.org/fediverse/fep/src/branch/main/fep/fffd/fep-fffd.md)

Enable `jekyll-activity-pub-fep-fffd-distributed-press` to generate
alternate URLs for actor and activities.

## FEP fffd: Licenses

If you have a page or a post with layout "license" and enable
`jekyll-activity-pub-fep-fffd-license`, every activity will link to its
license.

### FEP 1042: Peer to Peer Fediverse Activities

Enabling `jekyll-activity-pub-fep-1042` (or
`jekyll-activity-pub-fep-fffd-distributed-press` for backwards
compatibility)  will add [Distributed Press](https://distributed.press/)
addresses as alternate URLs for each activity and actor.

It runs on `pre_render` so every activity associated with the Actor is
initialized (specially Outbox and Following collections).
