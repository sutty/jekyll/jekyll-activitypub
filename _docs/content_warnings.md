---
title: Content warnings
layout: post
---

Content warnings are a way to indicate your readers your article
contains mentions to sensitive or triggering topics and decide if they
want to read it.  They can be achieved in two ways.

## First way

Your post can contain two independent attributes, `sensitive` and
`summary`.  These names follow the attribute naming for ActivityPub.
`sensitive` is a boolean, and `summary` a string.

If you fill `summary` but not `sensitive`, or `sensitive` is `false`,
Mastodon and other clients will "compact" the contents, and people can
click a "read more" button to access the content.  With `sensitive` as
`true`, they will also prefix the `summary` with a "CW:".

Since it's generally a good practice to provide a summary for long-form
text, this plugin always uses the post title as part of the summary and
adds a specific summary when present.

You can modify this via hook plugins.

Example:

```yaml
---
title: This is a post
summary: With a summary
sensitive: true
---

Here I'm going to be talking some _stuff_.
```

## Second way

A shorter format to the first way is to use a `content_warning`
attribute which performs the two functions at the same time.  When
a `content_warning` is provided as text, this plugin will automatically
set the `sensitive` attribute to `true` and use its contents as the
`summary`.

Example:

```yaml
---
title: This is a post
content_warning: stuff
---

Here I'm going to be talking some _stuff_.
```
