---
title: Instance
layout: post
---

[/api/v1/instance](https://docs-p.joinmastodon.org/methods/instance/) is
an API method to discover information about the server, following
Mastodon's API.

This file is auto-generated and used by Fediverse crawlers to find
information about your site.

## Configuration options

```yaml
# _config.yml
url: "https://examp.le"
title: "title (fallback)"
tagline: "description (fallback 1)"
description: "description (fallback 2)"
logo: "avatar (fallback)"
email: "email (fallback)"
image:
  path: "header (fallback)"
activity_pub:
  public_name: "title"
  summary: "description"
  email: "email"
  icons:
  - "avatar"
  images:
  - "header"
locales:
- "locale"
- "locale 2"
```

## Plugin

You can modify this information by hooking the Instance generation:

```ruby
# _plugins/instance.rb

# Registrations are open! (?)
Jekyll::Hooks.register :instance, :post_init do |instance|
  instance.data['registrations']['enabled'] = true
end
```
