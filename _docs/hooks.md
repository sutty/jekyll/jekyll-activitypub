---
title: Hooks
layout: post
---

> Using hooks, your plugin can exercise fine-grained control over
> various aspects of the build process. If your plugin defines any
> hooks, Jekyll will call them at pre-defined points. --
> [Hooks](https://jekyllrb.com/docs/plugins/hooks/)

This plugin adds the following hook points:

| Owner             | Event        | Triggered at                             |
| ----------------- | ------------ | ---------------------------------------- |
| `:actor`          | `:post_init` | After creating an Actor profile          |
| `:webfinger`      | `:post_init` | After creating a Webfinger               |
| `:activity`       | `:post_init` | After converting a post into an activity |
| `:property_value` | `:post_init` | After creating a property value          |
| `:image`          | `:post_init` | After creating an image attachment       |
| `:create`         | `:post_init` | After creating a create activity         |
| `:update`         | `:post_init` | After creating an update activity        |
| `:outbox`         | `:post_init` | After creating an outbox                 |

Also, since most of these are also pages, you can hook on regular
`:pages` events.

## Examples

### Add a custom property value to your profile

```ruby
# _plugins/add_custom_property_value_to_profile.rb

Jekyll::Hooks.register :actor, :post_init do |actor|
  actor.data['attachment'] << Jekyll::ActivityPub::PropertyValue.new('Pronouns', 'he/him')
end
```
