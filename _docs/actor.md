---
title: Actor
layout: post
---

The Actor represents your site as an ActivityPub user, so it can be
mentioned and Activities listed.

## Configuration

Configuration can be done under an `activity_pub` section on
`_config.yml`.  Missing or empty keys will be guessed using
`jekyll-seo-tag` configuration, so you can customize for Fediverse or
reuse the SEO configuration.

```yaml
title: "public name"
tagline: "summary"
description: "summary"
logo: "default_avatar.png"
activity_pub:
  public_name: "public name, site title if missing"
  manually_approves_followers: false
  summary: "profile summary, tagline or description if missing"
  icons:
  - "actor_avatar.png"
  images:
  - "actor_banner.png"
  published: TIMESTAMP
  updated: TIMESTAMP
```

See WebFinger docs for other options.

`published` and `updated` are optional timestamp strings you can use to
customize when the actor profile has been modified. By default, they're
the site's build time.

## Auto-accept follow requests

Change `manually_approves_followers` to `false` so you don't have to
moderate every follow request.  By default every follow request is
moderated.

## Customization

The Actor page is generated as a low priority site `post_read` hook.
You can customize its `data` hash.

```ruby
# _plugins/activity_pub.rb
Jekyll::Hooks.register :actor, :post_init do |actor|
  actor.data['summary'] = "I'm customizing the summary"
end
```
