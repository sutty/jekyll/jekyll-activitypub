---
title: Social Distributed Press
layout: post
---

AKA Social Inbox.

## Install

To enable these commands, you'll need to add them to your `Gemfile` on
an early plugins load:

```ruby
source 'https://rubygems.org'

gem 'jekyll', '~> 4'

group :jekyll_plugins do
  gem 'jekyll-activity-pub', require: 'jekyll/activity_pub/commands'
end
```

## Configure

See Inbox

## Generating a private key

Keys are used to sign requests to [Social Distributed
Press](https://social.distributed.press/).  The key is also uploaded, so
it can sign requests to other instances on your behalf.

```sh
# Flags are optional
bundle exec jekyll generate_keys --key rsa.pem --key-size 2048
```

This will create a private key file. **You MUST NOT commit this or
otherwise make it public.**  But you can regenerate it everytime.

## Building the website

There's a new flag for `jekyll build`:

```sh
bundle exec jekyll build --key rsa.pem
```

It's needed to extract the public key during build and add it to the
actor object (your site's "profile).

## Verification process

To verify the key belongs to your site, you need to publish the site
first containing the public key and then send the Social Distributed
Press instance an inbox creation request.  The inbox will fetch the
public key and compare it with what you're providing.

Thus, you need to run this process after the files are correctly
propagated.

This is done automatically by the `jekyll notify` command, as long as
you provide it with the public key.

## Sending notifications

Notifications are sent once you are sure your site has been published
and propagated.

```sh
bundle exec jekyll notify --key /path/to/private.key
```

During this step:

1. Inbox is created on your Social Distributed Press instance.
2. Public key publication is validated.
