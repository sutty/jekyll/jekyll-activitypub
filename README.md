# Jekyll ActivityPub

Generates an [ActivityPub](https://www.w3.org/TR/activitypub/)
representation of your site and delegates notifications and followers to
the [Distributed Press](https://distributed.press) Social Inbox.

## Installation and Usage

See _docs/

## Development

After checking out the repo, run `bundle` to install dependencies.

To release a new version, update the version number in
`jekyll-activity-pub.gemspec`, and then run `go-task release`, which
will push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on 0xacab.org at
<https://0xacab.org/sutty/jekyll/jekyll-activity-pub>. This
project is intended to be a safe, welcoming space for collaboration, and
contributors are expected to adhere to the [Sutty code of
conduct](https://sutty.nl/en/code-of-conduct/).

## License

The gem is available as free software under the terms of the Apache2
License.

## Code of Conduct

Everyone interacting in the jekyll-activity-pub project’s
codebases, issue trackers, chat rooms and mailing lists is expected to
follow the [code of conduct](https://sutty.nl/en/code-of-conduct/).
