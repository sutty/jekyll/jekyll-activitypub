# frozen_string_literal: true

require 'nokogiri'

# Parses the Activity contents as HTML5 so we can modify it later.
# Priority is highest so we always have the parsed contents.
Jekyll::Hooks.register :activity, :post_init, priority: 100 do |activity|
  case activity.data['content']
  when String
    Jekyll.logger.debug 'ActivityPub:', "Parsing content for #{activity.data['id']}"

    activity.data['content'] =
      activity.data['contentMap'][activity.locale] =
      ::Nokogiri::HTML5.fragment(activity.data['content'])
  end
end
