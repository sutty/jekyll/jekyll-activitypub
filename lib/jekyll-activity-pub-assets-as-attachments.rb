# frozen_string_literal: true

require_relative 'jekyll-activity-pub-absolute-assets'

# Convert all embedded images, videos and audios into attachments. Note
# Mastodon only supports four attachments per post.
#
# The hook run after absolute assets.
Jekyll::Hooks.register :activity, :post_init, priority: 34 do |activity|
  activity.data['content'].css('img[src],video[src],audio[src]').each do |element|
    src = Addressable::URI.parse(element['src'])

    if src.origin == activity.site.config['url']
      Jekyll.logger.info 'ActivityPub:', "#{src.path}: Adding media as attachment"

      activity.data['attachment'] << Jekyll::ActivityPub::Document.new(activity.site, src.path, element['alt'].to_s)
    else
      Jekyll.logger.warn 'ActivityPub:', "#{src}: Can't add remote media as attachment yet"
    end
  end
end
