# frozen_string_literal: true

require 'jekyll/page'
require_relative 'helper'
require_relative 'notifier'

module Jekyll
  module ActivityPub
    # Public key
    class PublicKey < Jekyll::Page
      include Helper

      # Initialize with default data
      #
      # @param :site [Jekyll::Site]
      # @param :actor [Jekyll::ActivityPub::Actor]
      # @param :public_key [OpenSsl::Pkey::RSA]
      # @param :base [String]
      # @param :dir [String]
      # @param :name [String]
      def initialize(site, actor, public_key, base = '', dir = '', name = 'pubkey.pem')
        @context = StubContext.new(registers: { site: site })
        @actor = actor
        @public_key = public_key

        super(site, base, dir, name)

        actor.data['publicKey'] = self

        Notifier.public_key_url = data['id']

        trigger_hooks :post_init
      end

      def read_yaml(*)
        self.data = {
          'id' => "#{absolute_url(@actor.url)}#main-key",
          'owner' => absolute_url(@actor.url),
          'publicKeyPem' => @public_key.public_to_pem
        }
      end

      def content
        data['publicKeyPem']
      end
    end
  end
end
