# frozen_string_literal: true

module Jekyll
  module ActivityPub
    VERSION = '0.1.0'
  end
end
