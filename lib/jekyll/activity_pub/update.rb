# frozen_string_literal: true

require_relative 'create'

module Jekyll
  module ActivityPub
    # Represents an Update activity
    class Update < Create
      DATE_ATTRIBUTE = 'updated'
    end
  end
end
