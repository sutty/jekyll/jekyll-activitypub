# frozen_string_literal: true

require 'pathname'
require 'httparty'
require 'distributed_press/version'
require 'distributed_press/v1/social/client'
require 'distributed_press/v1/social/inbox'
require 'distributed_press/v1/social/outbox'
require 'distributed_press/v1/social/replies'
require 'distributed_press/v1/social/dereferencer'
require 'distributed_press/v1/social/shares'
require 'distributed_press/v1/social/likes'
require_relative 'errors'
require_relative 'create'
require_relative 'update'
require_relative 'delete'

module Jekyll
  module ActivityPub
    # Long term store for notifications.
    #
    # Needs to be a singleton so we can use the same data across all of
    # Jekyll's build process.
    class Notifier
      # An struct that behaves like a page
      PseudoObject = Struct.new(:url, :data, :site, :relative_path, keyword_init: true) do
        def destination(_)
          site.in_dest_dir(relative_path)
        end

        def date
          @date ||= Time.parse(data['published'])
        rescue StandardError
          nil
        end

        # @return [Time, nil]
        def updated_at
          @updated_at ||= Time.parse(data['updated'])
        rescue StandardError
          nil
        end
      end

      class << self
        # Set the site and initialize data
        #
        # @param :site [Jekyll::Site]
        # @return [Jekyll::Site]
        def site=(site)
          @@site = site.tap do |s|
            s.data['activity_pub'] ||= {}
            s.data['activity_pub']['notifications'] ||= {}
          end
        end

        # @return [Jekyll::Site]
        def site
          @@site
        end

        def dereferencer
          @@dereferencer ||= DistributedPress::V1::Social::Dereferencer.new(client: client)
        end

        # Announce the website?
        def announce?
          !!config['announce']
        end

        def manually_approves_followers?
          !!config['manually_approves_followers']
        end

        def url
          config['url'].tap do |u|
            abort_if_missing('activity_pub.url', u, '_config.yml')
          end
        end

        # @return [String]
        def followers_url
          "#{url}/v1/#{actor}/followers"
        end

        # Save the public key URL for later
        #
        # @param :url [String]
        def public_key_url=(url)
          data['public_key_url'] = url
        end

        # Public key URL, raises error if missing
        #
        # @return [String]
        def public_key_url
          data['public_key_url'].tap do |pk|
            abort_if_missing('public_key_url', pk)
          end
        end

        def actor=(actor)
          data['actor'] = actor
        end

        def actor
          data['actor'].tap do |a|
            abort_if_missing('actor', a)
          end
        end

        def actor_url=(url)
          data['actor_url'] = url
        end

        def actor_url
          data['actor_url'].tap do |au|
            abort_if_missing('actor_url', au)
          end
        end

        # Send notifications
        #
        # 1. Wait for public key propagation
        # 2. Create/update inbox
        # 3. Send create, update, and delete
        def notify!
          # TODO: request several times with a timeout
          response = HTTParty.get(public_key_url)

          unless response.ok?
            raise NotificationError,
                  "Could't fetch public key (#{response.code}: #{response.message}). It needs to be available at #{public_key_url} before notifications can work, because it's used for signature verification."
          end

          unless client.private_key.compare? OpenSSL::PKey::RSA.new(response.parsed_response.dig('publicKey', 'publicKeyPem'))
            raise NotificationError, "Public key at #{public_key_url} differs from local version"
          end

          actor_object = object_for(site.in_dest_dir(URI.parse(actor_url).path))

          # Create/Update inbox
          unless (response = inbox.create(actor_url, announce: announce?, manually_approves_followers: manually_approves_followers?)).ok?
            raise NotificationError, "Couldn't create/update inbox (#{response.code}: #{response.message})"
          end

          # Remove notifications already performed and notify
          data['notifications'].reject do |object_url, _|
            done? object_url
          end.each do |object_url, status|
            process_object(actor_object, object_for(object_url), status)
          end

          # Update actor profile
          if actor_object.updated_at > actor_object.date
            Jekyll.logger.debug 'ActivityPub:', 'Updating Actor profile'
            actor_update = Jekyll::ActivityPub::Update.new(site, actor_object, actor_object)

            unless (response = outbox.post(activity: actor_update)).ok?
              raise NotificationError, "Couldn't update actor (#{response.code}: #{response.message})"
            end
          end

          # Store everything for later
          save
        rescue NotificationError => e
          Jekyll.logger.abort_with 'ActivityPub:', e.message
        end

        # Return data
        #
        # @return [Hash]
        def data
          @@data ||= site.data['activity_pub']
        end

        # Removes an activity if it was previously created
        #
        # @param :path [String]
        # @return [nil]
        def delete(path)
          action(path, 'delete') if exist?(path) && !deleted?(path)
        end

        # Updates an activity if it was previously created, otherwise
        # create it, or update it again if it was modified later
        #
        # @param :path [String]
        # @return [nil]
        def update(path, **opts)
          # Compare Unix timestamps
          if created?(path) && (object_for(path)&.updated_at&.to_i || 0) > (status(path)['updated_at'] || 0)
            action(path, 'update', **opts)
          else
            create(path, **opts)
          end

          nil
        end

        # Creates an activity unless it has been already created
        #
        # @param :path [String]
        # @return [nil]
        def create(path, **opts)
          action(path, 'create', **opts) unless created?(path)

          nil
        end

        # Gets status
        #
        # @param :path [String]
        # @return [Hash]
        def status(path)
          data['notifications'][path_relative_to_dest(path)] || {}
        end

        # Detects if an action was already done
        #
        # @param :path [String]
        def done?(path)
          (status(path)['action'] == 'done').tap do |done|
            Jekyll.logger.debug('ActivityPub:', "Skipping notification for #{path}") if done
          end
        end

        # Already created
        #
        # @param :path [String]
        def created?(path)
          !status(path)['created_at'].nil?
        end

        # @param :path [String]
        def updated?(path)
          !status(path)['updated_at'].nil?
        end

        # @param :path [String]
        def deleted?(path)
          !status(path)['deleted_at'].nil?
        end

        def exist?(path)
          !status(path)['action'].nil?
        end

        # Stores data back to a file and optionally commits it
        #
        # @return [nil]
        def save
          # TODO: Send warning if CI is detected
          Jekyll.logger.info 'ActivityPub:', "Saving data to #{relative_path}"

          FileUtils.mkdir_p(File.dirname(path))

          File.open(path, 'w') do |f|
            f.flock(File::LOCK_EX)
            f.rewind
            f.write(YAML.dump(data.reject { |_, v| v.empty? }))
            f.flush
            f.truncate(f.pos)
          end

          if ENV['JEKYLL_ENV'] == 'production' && site.respond_to?(:repository)
            site.staged_files << relative_path
            site.repository.commit 'ActivityPub'
          end

          nil
        end

        # Returns the path for the storage
        #
        # @return [String]
        def path
          @@path ||= site.in_source_dir(site.config['data_dir'], 'activity_pub.yml')
        end

        # Storage path relative to site source
        #
        # @return [String]
        def relative_path
          @@relative_path ||= Pathname.new(path).relative_path_from(site.source).to_s
        end

        def client
          @@client ||= DistributedPress::V1::Social::Client.new(
            private_key_pem: private_key,
            url: url,
            public_key_url: public_key_url,
            logger: Jekyll.logger
          )
        end

        def inbox
          @@inbox ||= DistributedPress::V1::Social::Inbox.new(client: client, actor: actor)
        end

        def outbox
          @@outbox ||= DistributedPress::V1::Social::Outbox.new(client: client, actor: actor)
        end

        # Generates a Replies client per activity
        #
        # @param activity [String] Activity ID
        # @return [DistributedPress::V1::Social::Replies]
        def replies(activity)
          @@replies ||= {}
          @@replies[activity] ||= DistributedPress::V1::Social::Replies.new(client: client, actor: actor, activity: activity)
        end

        # Generates a Likes client per activity
        #
        # @param activity [String] Activity ID
        # @return [DistributedPress::V1::Social::Likes]
        def likes(activity)
          @@likes ||= {}
          @@likes[activity] ||= DistributedPress::V1::Social::Likes.new(client: client, actor: actor, activity: activity)
        end

        # Generates a Shares client per activity
        #
        # @param activity [String] Activity ID
        # @return [DistributedPress::V1::Social::Shares]
        def shares(activity)
          @@shares ||= {}
          @@shares[activity] ||= DistributedPress::V1::Social::Shares.new(client: client, actor: actor, activity: activity)
        end

        private

        # Finds the private key path on config
        #
        # @return [String, nil]
        def private_key_path
          @@private_key_path ||= site.config['activity_pub_private_key'].tap do |pk|
            abort_if_missing '--key', pk, 'notify command'
          end
        end

        # Returns the private key
        #
        # @return [String, nil]
        def private_key
          @@private_key ||= File.read private_key_path
        rescue StandardError
          Jekyll.logger.abort_with 'ActivityPub:', 'There\'s an issue with your private key'
        end

        # @return [Hash]
        def config
          @@config ||= site.config['activity_pub'] || {}
        end

        # Run action
        #
        # @param :path [String]
        # @param :action [String]
        # @return [nil]
        def action(path, action, **opts)
          path = path_relative_to_dest(path)

          data['notifications'][path] ||= {}
          data['notifications'][path]['action'] = action.to_s
          data['notifications'][path].merge! opts.transform_keys(&:to_s)

          nil
        end

        # Paths are relative to site destination
        #
        # @param :path [String]
        # @return [String]
        def path_relative_to_dest(path)
          @@path_relative_to_dest ||= {}
          @@path_relative_to_dest[path] ||= Pathname.new(site.in_dest_dir(path)).relative_path_from(site.dest).to_s
        end

        # Turns an object into an activity and notifies outbox
        #
        # @param :actor [PseudoObject]
        # @param :object [PseudoObject]
        # @param :status [Hash]
        # @return [nil]
        def process_object(actor, object, status)
          action = status['action']
          activity = Object.const_get("Jekyll::ActivityPub::#{action.capitalize}").new(site, actor, object)

          if (response = outbox.post(activity: activity)).ok?
            status['action'] = 'done'
            status["#{action}d_at"] = Time.now.to_i
          else
            Jekyll.logger.warn 'ActivityPub:',
                               "Couldn't perform #{action} for #{object.url} (#{response.code}: #{response.message})"
          end

          nil
        rescue NameError
          Jekyll.logger.warn 'ActivityPub:', "Action \"#{action}\" for #{object.url} unrecognized, ignoring."
        end

        def object_for(path)
          @@object_for ||= {}
          @@object_for[path_relative_to_dest(path)] ||=
            begin
              rel  = path_relative_to_dest(path)
              path = site.in_dest_dir(rel)
              if File.exist? path
                data = JSON.parse(File.read(path))
              else
                Jekyll.logger.warn 'ActivityPub:', "#{path} doesn't exist!"

                data = { 'id' => status(path)['id'] }
              end

              PseudoObject.new(url: rel, site: site, relative_path: rel, data: data)
            end
        end

        def abort_if_missing(key_name, value, file = '_data/activity_pub.yml')
          return unless value.nil? || value.empty?

          Jekyll.logger.abort_with 'ActivityPub:', "Missing #{key_name} in #{file}"
        end
      end
    end
  end
end
