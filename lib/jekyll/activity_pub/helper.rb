# frozen_string_literal: true

require 'forwardable'
require 'jekyll/hooks'

module Jekyll
  module ActivityPub
    # Container for common tools
    module Helper
      include Jekyll::Filters::URLFilters
      extend Forwardable

      # So we can deep dig between linked objects
      def_delegators :data, :dig

      # Some filters needs a Liquid-like context
      StubContext = Struct.new(:registers, keyword_init: true)

      # Convert into a dereferentiable object
      #
      # @return [Jekyll::Drops::ActivityDrop]
      def to_liquid
        @to_liquid ||=
          begin
            require 'jekyll/drops/activity_drop'

            Jekyll::Drops::ActivityDrop.new(self)
          end
      end

      # Removes empty values from data
      #
      # @return [Hash]
      def pruned_data
        data.reject do |_, v|
          v.nil? || (v.respond_to?(:empty?) && v.empty?)
        end
      end

      # Renders the data hash as a stringified JSON
      #
      # @return [String]
      def content
        pruned_data.to_json
      end

      # JSONify this object
      def to_json(*args)
        pruned_data.to_json(*args)
      end

      # There's no excerpt to be generated
      #
      # @return [Boolean]
      def generate_excerpt?
        false
      end

      # Avoid
      def render_with_liquid?
        false
      end

      # Avoid
      def place_in_layout?
        false
      end

      # Trigger hooks
      #
      # @param :hook_name [Symbol]
      # @param :args [any]
      # @return [nil]
      def trigger_hooks(hook_name, *args)
        Jekyll::Hooks.trigger hook_owner, hook_name.to_sym, self, *args
      end

      # Simpler version of ActiveSupport::Inflector#underscore
      #
      # https://stackoverflow.com/a/1510078
      #
      # @see Jekyll::Convertible
      # @return [Symbol]
      def hook_owner
        @hook_owner ||= self.class.name.split('::').last.gsub(/(.)([A-Z])/, '\1_\2').downcase.to_sym
      end

      alias type hook_owner

      # Detects locale
      #
      # @return [String]
      def locale
        return @locale if defined? @locale

        @locale   = site.config['locale']
        @locale ||= ENV['LANG']&.split('.', 2)&.first

        @locale = @locale&.tr('_', '-')
      end

      private

      # Is Liquid on strict mode?
      #
      # @return [Boolean]
      def strict?
        site.config.dig('liquid', 'strict_variables')
      end

      # Returns site name, for use as a username.  "site" by default.
      #
      # @return [String]
      def username
        @username ||= site.config.dig('activity_pub', 'username')
        @username ||= hostname&.split('.', 2)&.first
        @username ||= 'site'
      end

      # Finds public name
      #
      # @return [String,nil]
      def public_name
        @public_name ||= find_best_value_for(site.config, %w[activity_pub public_name], 'title') || username
      end

      # @return [Time]
      def published
        @published ||= site.config.dig('activity_pub', 'published') || site.time
      end

      # Find summary between site tagline or description, reusing fields
      # that are used on jekyll-seo-tag
      #
      # @return [String,nil]
      def summary
        @summary ||= find_best_value_for(site.config, %w[activity_pub summary], 'tagline', 'description')
      end

      # Return hostname
      #
      # @return [String]
      def hostname
        return @hostname if defined? @hostname

        @hostname   = site.config.dig('activity_pub', 'hostname')
        @hostname ||= site.config['hostname']
        @hostname ||= Addressable::URI.parse(site.config['url']).hostname if site.config['url']
        @hostname ||= File.read(cname_file).strip if cname_file?

        if @hostname.nil? || @hostname.empty?
          raise ArgumentError, 'Site must have a hostname' if strict?

          Jekyll.logger.warn 'ActivityPub:', 'Site must have a hostname'

        end

        @hostname
      end

      # Site uses CNAME file
      #
      # @return [String]
      def cname_file
        @cname_file ||= site.in_source_dir('CNAME')
      end

      # @return [Boolean]
      def cname_file?
        File.exist?(cname_file)
      end

      # Finds the value of a text field amongst options
      #
      # @params :hash [Hash] The haystack
      # @param :args [String,Array] A combination of paths to find
      # a value
      # @return [Any, nil]
      def find_best_value_for(hash, *args)
        raise ArgumentError, 'First argument must be hash' unless hash.is_a? Hash

        field = args.find do |f|
          !hash.dig(*f).nil? && !hash.dig(*f).empty?
        rescue TypeError
          false
        end

        hash.dig(*field) if field
      end

      # Raise an exception if the value is required but empty
      #
      # @param :value [String,nil]
      # @param :exception [Object]
      def value_is_required!(value, exception)
        raise exception if value.nil? || value.empty?
      rescue exception => e
        raise if strict?

        Jekyll.logger.warn 'ActivityPub:', e.message
      end

      # Generates an absolute URL if not empty
      #
      # @param :url [String,nil]
      # @return [nil,String]
      def conditional_absolute_url(url)
        return if url.nil? || url.empty?

        absolute_url url
      end
    end
  end
end
