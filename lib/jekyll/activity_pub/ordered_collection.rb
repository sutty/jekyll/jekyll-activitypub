# frozen_string_literal: true

require 'jekyll/page'
require_relative 'helper'
require_relative 'ordered_collection_page'

module Jekyll
  module ActivityPub
    # An ordered collection of activities
    class OrderedCollection < Jekyll::Page
      include Helper

      # Initialize with default data
      #
      # @param :site [Jekyll::Site]
      # @param :base [String]
      # @param :dir [String]
      # @param :name [String]
      def initialize(site, base = '', dir = '', name = 'ordered_collection.jsonld')
        @context = StubContext.new(registers: { site: site })

        super

        trigger_hooks :post_init
      end

      def read_yaml(*)
        self.data = {
          '@context' => 'https://www.w3.org/ns/activitystreams',
          'id' => absolute_url(url),
          'type' => 'OrderedCollection',
          'totalItems' => 0,
          'orderedItems' => []
        }
      end

      # Sort items
      #
      # @return [nil]
      def order_items!
        data['orderedItems'].sort! do |a, b|
          b.data['published'] <=> a.data['published']
        end

        nil
      end

      # Turn items to URLs
      #
      # @return [nil]
      def items_to_links!
        data['orderedItems'] = data['orderedItems'].map do |item|
          item.data['id']
        end

        nil
      end

      # Paginates the collection if it has too many activities
      #
      # @return [nil]
      def paginate!
        return if always_paginate? || paginable?
        return if data['orderedItems'].empty?

        paged_data = data.dup
        pages = paginate(paged_data.delete('orderedItems'))

        assign_links! pages

        paged_data['first'] = absolute_url(pages.first.url)
        paged_data['last'] = absolute_url(pages.last.url)

        nil
      end

      private

      # Items per page
      #
      # @return [Integer]
      def items_per_page
        @items_per_page ||=
          begin
            from_config = site.config.dig('activity_pub', 'items_per_page').to_i
            if from_config.positive?
              from_config
            else
              Jekyll.logger.warn 'ActivityPub:', 'Items per page option empty or not valid, using 20 items per page'
              20
            end
          end
      end

      # Force pagination
      #
      # @return [Boolean]
      def always_paginate?
        !!site.config.dig('activity_pub', 'always_paginate')
      end

      # Item count is higher than items per page
      #
      # @return [Boolean]
      def paginable?
        data['orderedItems'].size > items_per_page
      end

      # Generate pages
      #
      # @param :items [Array]
      # @return [Array]
      def paginate(items)
        [].tap do |pages|
          total_pages = items.count / items_per_page

          items.each_slice(items_per_page).each_with_index do |paged_items, i|
            OrderedCollectionPage.new(site, self, nil, basename, "#{total_pages - i}.jsonld").tap do |page|
              page.data['orderedItems'] = paged_items
              site.pages << page
              pages << page
            end
          end
        end
      end

      # Assign previous and next links between pages
      #
      # @param :pages [Array]
      # @return [nil]
      def assign_links!(pages)
        pages.reduce do |previous, current|
          current.data['prev'] = absolute_url(previous.url)
          previous.data['next'] = absolute_url(current.url)

          current
        end

        nil
      end
    end
  end
end
