# frozen_string_literal: true

require 'jekyll/page'
require_relative 'nodeinfo_20'

module Jekyll
  module ActivityPub
    # Points to site's nodeinfo 2.1
    class Nodeinfo21 < Nodeinfo20
      def version
        '2.1'
      end

      def read_yaml(*)
        super.tap do |data|
          data['software']['repository'] = 'https://0xacab.org/sutty/jekyll/jekyll-activity-pub/'
          data['software']['homepage'] = 'https://sutty.nl'
        end
      end
    end
  end
end
