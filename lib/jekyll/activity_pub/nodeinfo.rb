# frozen_string_literal: true

require 'jekyll/page'
require_relative 'helper'

module Jekyll
  module ActivityPub
    # Points to site's nodeinfo
    class Nodeinfo < Jekyll::Page
      include Helper

      # Initialize with default data
      #
      # @param :site [Jekyll::Site]
      # @param :nodeinfos [Array<Nodeinfo21>]
      # @param :base [String]
      # @param :dir [String]
      # @param :name [String]
      def initialize(site, nodeinfos, base = '', dir = '.well-known', name = 'webfinger')
        @context = StubContext.new(registers: { site: site })
        @nodeinfos = nodeinfos

        super(site, base, dir, name)

        trigger_hooks :post_init
      end

      def read_yaml(*)
        self.data = { 'links' => @nodeinfos.map(&:to_jrd) }
      end

      # The nodeinfo file is expected to be at this location always
      #
      # @return [String]
      def permalink
        '.well-known/nodeinfo'
      end
    end
  end
end
