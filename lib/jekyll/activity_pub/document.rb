# frozen_string_literal: true

require_relative 'image'

module Jekyll
  module ActivityPub
    # Represents a Document
    class Document < Image
      # Initialize with default data
      #
      # @param :site [Jekyll::Site]
      # @param :path [Path]
      # @param :description [String]
      def initialize(site, path, description = nil)
        super

        data['type'] = 'Document'

        trigger_hooks :post_init
      end
    end
  end
end
